require 'test_helper'

module App
  class SharedMessagesControllerControllerTest < ActionDispatch::IntegrationTest
    setup do
      @message = create(:message)
    end

    test 'successful with valid code' do
      get app_shared_message_path(id: @message.id, code: 'secret')
      assert_response :success
    end

    test 'not found with invalid code' do
      assert_raise ActionController::RoutingError do
        get app_shared_message_path(id: @message.id, code: 'wrong')
      end
    end
  end
end
