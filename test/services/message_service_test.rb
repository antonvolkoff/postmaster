require 'test_helper'

class MessageServiceTest < ActiveSupport::TestCase
  test '.list returns list of messages for today' do
    user = create(:user)
    message = create(:message, users: [user])

    messages = MessageService.list(user: user, time_frame: 'today')

    assert_equal [message], messages
  end

  test '.list returns list of messages by tag' do
    user = create(:user)
    tag = create(:tag)
    message = create(:message, users: [user], tags: [tag])

    messages = MessageService.list(
      user: user,
      time_frame: 'today',
      tags: [tag.name]
    )

    assert_equal [message], messages
  end
end
