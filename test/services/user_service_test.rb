require 'test_helper'

class UserServiceTest < ActiveSupport::TestCase
  setup do
    @inbox_tag = create(:tag, name: 'Inbox')
    @outbox_tag = create(:tag, name: 'Outbox')
  end

  test '.signup creates user' do
    assert_difference 'User.count', 1 do
      UserService.signup!(address: 'address@domain.com', password: 'secret')
    end
  end

  test '.signup! connects user with two tags' do
    params = { address: 'address@domain.com', password: 'secret' }

    user = UserService.signup!(params)

    assert_equal 2, user.tags.count
  end
end
