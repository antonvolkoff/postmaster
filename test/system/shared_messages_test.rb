require 'application_system_test_case'

class SharedMessagesTest < ApplicationSystemTestCase
  setup do
    @message = create(:message)
  end

  test 'message can be viewed when correct code is given' do
    visit app_shared_message_url(id: @message.id, code: 'secret')

    assert_text @message.parsed.html_body
  end
end
