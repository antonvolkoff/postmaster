require 'application_system_test_case'

class MailsTest < ApplicationSystemTestCase
  setup do
    @current_user = create(:user)
    @inbox_tag = create(:tag, name: 'Inbox')
    @outbox_tag = create(:tag, name: 'Outbox')
  end

  test 'user sees a list of emails' do
    message1 = create(:message, to: [@current_user.to], users: [@current_user])
    message2 = create(:message, to: [@current_user.to], users: [@current_user])
    message3 = create(:message)

    tag1 = create(:tag, users: [@current_user])
    tag2 = create(:tag, users: [@current_user])

    login(@current_user)
    visit app_root_url

    assert_text message2.parsed.subject
    assert_text message1.parsed.subject
    refute_text message3.parsed.subject

    assert_text tag1.name
    assert_text tag2.name
  end

  test 'user opens mail by clicking on it' do
    mail = Mail.new do
      from 'sender@example.com'
      to 'recepient@example.com'
      subject 'Click ME!'
      body 'Hello. This is an email.'
    end
    create(
      :message,
      data: mail.to_s,
      to: [@current_user.to],
      users: [@current_user]
    )

    login(@current_user)
    visit app_root_url
    mail_window = window_opened_by { click_link mail.subject }

    within_window mail_window do
      assert_text mail.body
    end
  end

  test 'user sees emails from yesterday' do
    message1 = create(:message,
                       to: [@current_user.to],
                       users: [@current_user],
                       created_at: 1.day.ago)
    message2 = create(:message,
                       to: [@current_user.to],
                       users: [@current_user],
                       created_at: Time.now)

    login(@current_user)
    visit app_root_url

    refute_text message1.parsed.subject
    assert_text message2.parsed.subject

    click_on 'Yesterday'

    assert_text message1.parsed.subject
    refute_text message2.parsed.subject

    click_on 'Today'

    refute_text message1.parsed.subject
    assert_text message2.parsed.subject
  end

  test 'user redirected to login page when not authenticated' do
    visit app_root_url

    assert_current_path singin_path
  end

  test 'user redirected from mail page to login page when not authenticated' do
    message = create(:message)

    visit app_message_url(message)

    assert_current_path signin_path
  end

  test 'user opens a new mail form' do
    login(@current_user)
    visit app_root_url

    click_on 'New Mail'

    assert_current_path new_app_message_path
  end

  test 'users creates a new mail' do
    login(@current_user)
    visit new_app_message_path

    fill_in 'message[to]',	with: 'someone@example.com'
    fill_in 'message[subject]',	with: 'This is a test subject'
    fill_in 'message[body]',	with: 'Hey, this is a test email!'

    click_on 'Send'

    assert_current_path app_messages_path

    assert_text 'This is a test subject'
  end
end
