require 'test_helper'

Capybara.raise_server_errors = false

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  # driven_by :selenium, using: :chrome, screen_size: [1400, 1400]
  driven_by :selenium_chrome_headless

  def login(current_user = nil)
    current_user = create(:user) if current_user.nil?

    visit singin_url

    fill_in 'address',	with: current_user.address
    fill_in 'password', with: current_user.password

    click_on 'Sign In'

    current_user
  end
end
