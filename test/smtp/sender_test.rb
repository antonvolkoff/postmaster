require 'test_helper'
require Rails.root.join('lib/smtp')

module SMTP
  class SenderTest < ActiveSupport::TestCase
    test '#deliver sends mail to SMTP server' do
      mail = Mail.new
      from = Mail::Address.new('from@outpostr.com')
      to = Mail::Address.new('to@outpostr.com')
      smtp = mock
      Net::SMTP.stubs(:start).returns(smtp)
      smtp.expects(:enable_starttls)
      smtp.expects(:send_message).with(mail.to_s, from.address, to.address)
      smtp.expects(:finish)

      SMTP::Sender.new('mail.example.com').deliver(
        mail: mail, from: from, to: to
      )
    end
  end
end
