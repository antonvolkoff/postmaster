require 'test_helper'
require Rails.root.join('lib/smtp')

module SMTP
  class ServerTest < ActiveSupport::TestCase
    setup do
      @server = SMTP::Server.new
      @inbox_tag = create(:tag, name: 'Inbox')
      @outbox_tag = create(:tag, name: 'Outbox')
    end

    test 'saves incoming message in database' do
      user = create(:user, address: 'to@example.com')
      mail = Mail.new do
        from 'from@example.com'
        to 'to@example.com'
        subject 'Test'
        body 'Hello'
      end
      context = {
        envelope: { from: '<from@example.com>', to: ['<to@example.com>'] },
        message: { data: mail.to_s }
      }

      @server.on_message_data_event(context)

      assert_equal 1, Message.count
      message = Message.last
      assert_equal 'from@example.com', message.from
      assert_equal ['to@example.com'], message.to
      assert_equal mail.to_s, message.data

      assert_equal 1, user.messages.count
    end

    test 'does not process message without data' do
      context = {
        envelope: { from: 'from@example.com', to: ['to@example.com'] },
        message: { data: nil }
      }

      @server.on_message_data_event(context)

      assert_equal 0, Message.count
    end

    test 'sends outgoing message' do
      user = create(:user, address: 'from@outpostr.com')
      mail = Mail.new do
        from 'from@outpostr.com'
        to 'to@outpostr.com'
        subject 'Test'
        body 'Hello'
      end
      context = {
        envelope: { from: '<from@outpostr.com>', to: ['<to@outpostr.com>'] },
        message: { data: mail.to_s }
      }

      @server.on_message_data_event(context)

      assert_equal 0, Message.count
    end

    test 'sends outgoing message when user unknown' do
      mail = Mail.new do
        from 'from@outpostr.com'
        to 'to@outpostr.com'
        subject 'Test'
        body 'Hello'
      end
      context = {
        envelope: { from: '<from@outpostr.com>', to: ['<to@outpostr.com>'] },
        message: { data: mail.to_s }
      }

      @server.on_message_data_event(context)

      assert_equal 0, Message.count
    end
  end
end
