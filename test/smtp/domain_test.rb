require 'test_helper'
require Rails.root.join('lib/smtp')

module SMTP
  class DomainTest < ActiveSupport::TestCase
    test '#exchanges returns list of mx exchanges' do
      domain = SMTP::Domain.new(email: 'anton@outpostr.com')

      result = domain.exchanges

      assert_equal ['mailbox.outpostr.com'], result
    end
  end
end
