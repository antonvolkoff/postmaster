require 'test_helper'

class TimeRangeTest < ActiveSupport::TestCase
  setup do
    @today = TimeRange.new('today')
    @yesterday = TimeRange.new('yesterday')
    @this_week = TimeRange.new('this_week')
    @last_week = TimeRange.new('last_week')
  end

  test 'should not init with unsupported time frame' do
    assert_raise 'Unsupported time frame' do
      TimeRange.new('this_month')
    end
  end

  test '#starts_at works' do
    freeze_time do
      assert_equal Time.now.utc.beginning_of_day, @today.starts_at
      assert_equal Time.now.utc.beginning_of_day - 1.day, @yesterday.starts_at
      assert_equal Time.now.utc.at_beginning_of_week, @this_week.starts_at
      assert_equal Time.now.utc.beginning_of_week - 7.days, @last_week.starts_at
    end
  end

  test '#ends_at works' do
    freeze_time do
      assert_equal Time.now.utc, @today.ends_at
      assert_equal Time.now.utc.beginning_of_day, @yesterday.ends_at
      assert_equal Time.now.utc.at_end_of_week, @this_week.ends_at
      assert_equal Time.now.utc.at_end_of_week - 7.days, @last_week.ends_at
    end
  end
end
