# frozen_string_literal: true

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test '#to returns string for query' do
    user = build(:user)

    assert "<#{user.address}>", user.to
  end
end
