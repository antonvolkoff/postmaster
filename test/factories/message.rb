FactoryBot.define do
  factory :message do
    from  { 'sender@example.com' }
    to    { ['recepient@example.com'] }

    sequence(:data) do |n|
      Mail.new do
        from    'Sender <sender@example.com>'
        to      'recepient@example.com'
        subject "This is a test email #{n}"
        body    'This is test email body.'
      end.to_s
    end
  end
end
