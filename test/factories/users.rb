FactoryBot.define do
  factory :user do
    address { 'admin@outpostr.com' }
    password { 'secret' }
  end
end
