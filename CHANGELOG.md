## [0.4.24]
### Change
- Do not validate TLS certificate

## [0.4.23]
### Added
- TLS support on SMTP server

## [0.4.22]
### Added
- JS filter of time frames

## [0.4.21]
### Added
- Tags support for mails

## [0.4.20]
### Added
- SMTP server sends emails

## [0.4.19]
### Change
- Update config

## [0.4.18]
### Added
- Simple form for sending mails

## [0.4.17]
### Change
- Fix bug missing message object

## [0.4.16]
### Change
- Fix bug with assigning user

## [0.4.14]
### Change
- Make raw mail a message

## [0.4.13]
### Added
- Connect user to message

## [0.4.12]
### Added
- Messages are parsed with Addresses

## [0.4.11]
### Added
- Message model that stores parsed data from RawMail
- Rake task to convert old RawMails

## [0.4.10]
### Changed
- New card design

## [0.4.9]
### Changed
- Style of mail list

## [0.4.8]
### Fixed
- User#to should not be wrapped as array

## [0.4.7]
### Change
- Look for users mails by address with "<" and ">"

## [0.4.6]
### Change
- Show emails that belongs only to users account

## [0.4.5]
### Change
- Bypass kafka and save mails to postgres

## [0.4.4]
### Change
- Precompile assets inside docker image

## [0.4.3]
### Change
- Package yarn into docker image

## [0.4.2]
### Change
- Package nodejs into docker image

## [0.4.1]
### Add
- Uglifier gem for `rake assets:precompile`

## [0.4.0]
### Add
- Authentication with password
- Add tailwindcss
- Home page
- Add web version of app

### Change
- Convert API app to full app

## [0.3.3]
### Change
- Update config for racecar gem. We need to make sure it uses same env variables.

## [0.3.2]
### Change
- Change kafka config. Allow to pass database data via ENV

## [0.3.1]
### Change
- Change database config. Allow to pass database data via ENV

## [0.3.0]
### Added
- Include name of sender to /api/mails response

### Changed
- Replaced haraka with smtpmidiserver written in ruby
- Use kafka instead of rabbitmq

### Removed
- RPC api controllers

### Fixed
- NoMethodError in MailsController#show when mail does not have html body

## [0.2.0]
### Changed
- Response from /api/mails endpoint includes display name of a sender
- Refactored Email class to use SimpleDelegator
- Changed connection pool size

## [0.1.2] - 26.02.2019
### Added
- RabbitMQ worker to accept new mails

## [0.1.1] - 25.02.2019
### Fixed
- Exception when parsing nested multipart mails

## [0.1.0] - 25.02.2019
### Add
- /api/mails endpoint to fetch emails
- /api/mails/:id endpoint to fetch HTML body of the email
- Sentry integration

### Changed
- Moved `InboxController` under `Rpc` module
- Set CORS headers to allow calls