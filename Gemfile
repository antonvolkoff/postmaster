source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.3'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.2'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma', '~> 3.12'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 4.0'

gem 'nokogiri', '~> 1.10.4'
gem 'rubyzip', '>= 1.3.0'

# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Applicaiton gems
gem 'ahoy_matey', '~> 3.0'
gem 'bcrypt', '~> 3.1.7'
gem 'sentry-raven', '~> 2.9'
gem 'skylight', '~> 4.1'
gem 'mail', '~> 2.7'
gem 'midi-smtp-server', '~> 2.3'
gem 'rack-cors', '~> 1.0'
gem 'rollout', '~> 2.4'
gem 'webpush', '~> 0.3'
gem 'wisper', '~> 2.0'
gem 'interactor', '~> 3.0'

# Frontend stuff
gem 'turbolinks', '~> 5.2'
gem 'webpacker', '~> 4.2'
gem 'uglifier', '~> 4.1'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false


group :development, :test do
  gem 'pry', '~> 0.12'
  # gem 'rspec-rails', '~> 3.8'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'capybara', '~> 3.12'
  gem 'factory_bot_rails', '~> 4.11'
  gem 'selenium-webdriver', '~> 3.142'
  gem 'chromedriver-helper', '~> 2.1'
  gem 'mocha', '~> 1.8'
end
