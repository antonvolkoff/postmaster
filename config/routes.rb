Rails.application.routes.draw do
  namespace :api do
    resources :mails, only: [:create]
    resources :messages, only: [:index] do
      resources :attachments, only: [:show]
    end
    resources :contacts, only: [:index]

    post '/ping', to: 'commands#ping'
    post '/message-list', to: 'commands#message_list'
    post '/message-get', to: 'commands#message_get'
    post '/message-create', to: 'commands#message_create'
    post '/web-notification-enable', to: 'commands#web_notification_enable'
    post '/web-notification-disable', to: 'commands#web_notification_disable'
    post '/web-notification-status', to: 'commands#web_notification_status'
  end

  namespace :app do
    root 'messages#index'
    resources :messages, only: [:index, :show, :new, :create] do
      resource :content, only: [:show]
    end
    resources :shared_messages, only: [:show]
    resource :web_notification, only: [:create, :destroy]

    resource :settings, only: [:show, :update]
  end

  namespace :app_v2 do
    root 'pages#index'
    get '*path', to: 'pages#index'
  end


  resources :wannabe_users, only: [:create]

  get   '/signup',  to: 'users#new'
  post  '/signup',  to: 'users#create'

  get   '/signin',  to: 'sessions#new'
  post  '/signin',  to: 'sessions#create'
  post  '/signout', to: 'sessions#destroy'

  get   '/service-worker.js', to: 'service_worker#service_worker'
  get   '/manifest.json',     to: 'service_worker#manifest'

  get   '/:id', to: 'pages#show', as: :page

  root  'pages#show', id: 'home'
end
