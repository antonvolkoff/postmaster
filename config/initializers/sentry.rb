# frozen_string_literal: true

dsn =
  if Rails.env.production?
    'https://02d585631d054feab561d31d93e7e32b:765d7a45840649198a1a1480e4cc2b63@sentry.io/1401775'
  else
    'dummy://catch.me/if/you/can'
  end

require 'raven/transports/dummy'

Raven.configure do |config|
  config.dsn = dsn
end