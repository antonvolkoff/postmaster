host = ENV['DEFAULT_URL_HOST'] || 'localhost:3000'
protocol = Rails.application.config.force_ssl ? 'https' : 'http'

Rails.application.routes.default_url_options.merge!(
  host: host,
  protocol: protocol,
)