Mail.defaults do
  if Rails.env.development? || Rails.env.test?
    delivery_method :logger, severity: :debug
  else
    delivery_method :smtp,
                    address: 'mailbox.outpostr.com',
                    port: 25,
                    openssl_verify_mode: 'none'
  end
end
