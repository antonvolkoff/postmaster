namespace :messages do
  task generate: :environment do
    user = User.first

    mail = Mail.new
    mail.from = 'sender@example.com'
    mail.to = user.address
    mail.subject = 'This is a subject'
    mail.body = 'This is a body'

    message = Message.create!(from: mail.from, to: mail.to, data: mail.to_s)
    message.assign!(user)

    tag = Tag.find_by(name: 'Inbox')
    message.message_tags.create!(tag: tag)
  end
end
