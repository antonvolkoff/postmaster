namespace :contacts do
  desc 'Populate contacts from messages'
  task populate: :environment do
    Contact::Populate.call
  end
end
