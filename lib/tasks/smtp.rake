namespace :smtp do
  desc 'Start and run SMTP server'
  task run: :environment do
    require Rails.root.join('lib/smtp')

    bind_port = '2525'
    bind_host = '0.0.0.0'
    max_conn = MidiSmtpServer::DEFAULT_SMTPD_MAX_PROCESSINGS
    opts = { tls_mode: :TLS_OPTIONAL }
    server = SMTP::Server.new(bind_port, bind_host, max_conn, opts)

    trap('INT') do
      puts 'Interrupted, exit now...'
      exit 0
    end

    at_exit do
      if server
        # Output for debug
        puts "#{Time.now}: Shutdown SMTP Server..."
        # stop all threads and connections gracefully
        server.stop
      end
      # Output for debug
      puts "#{Time.now}: SMTP Server down!"
    end

    puts "#{Time.now}: Starting SMTP Server"
    server.start
    server.join
  end
end
