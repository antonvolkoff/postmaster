class DeliverMessage
  def on_create_message_ok(message_id)
    message = Message.find(message_id)
    return if message.nil?

    deliver_to_all(message: message, addresses: message.to)
    deliver_to_all(message: message, addresses: message.parsed.cc)
  end

  private

  def deliver_to_all(message:, addresses:)
    return if addresses.nil?

    addresses.each do |address|
      deliver(
        mail_from: message.from,
        rcpt_to: address.to_s,
        data: message.data
      )
    end
  end

  def deliver(mail_from:, rcpt_to:, data:)
    my_hostname = 'mailbox.outpostr.com'
    port = 25
    hostname = Rails.application.credentials.smtp_relay[:hostname]
    username = Rails.application.credentials.smtp_relay[:username]
    password = Rails.application.credentials.smtp_relay[:password]

    smtp = Net::SMTP.start(hostname, port, my_hostname)
    smtp.enable_starttls
    smtp.authenticate(username, password)

    smtp.send_message(data, mail_from, rcpt_to)

    smtp.finish
  end
end