module OneTimeOpenToken
  TOKEN_LENGTH = 10

  module_function

  def generate(message_id)
    OpenToken
      .create!(message_id: message_id, value: SecureRandom.hex(TOKEN_LENGTH))
      .value
  end

  def valid?(message_id, value)
    token = OpenToken.find_by(message_id: message_id, value: value)
    if token
      token.delete
      true
    else
      false
    end
  end
end