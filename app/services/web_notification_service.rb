module WebNotificationService
  module_function

  def enable(user_id, params)
    object = WebNotification.find_by(user_id: user_id)
    object = WebNotification.new(user_id: user_id) if object.nil?

    object.update(params)

    object
  end

  def disable(user_id)
    object = WebNotification.find_by(user_id: user_id)
    return if object.nil?

    object.destroy
  end

  def send(user_ids, message)
    user_ids.each { |user_id| send_one(user_id, message) }
  end

  def send_one(user_id, message)
    web_notification = WebNotification.find_by(user_id: user_id)
    return if web_notification.nil?

    Webpush.payload_send(
      message: message,
      endpoint: web_notification.endpoint,
      p256dh: web_notification.p256dh_key,
      auth: web_notification.auth_key,
      vapid: {
        subject: "mailto:anton@outpostr.com",
        public_key: Rails.application.credentials.vapid_public_key,
        private_key: Rails.application.credentials.vapid_private_key
      },
      ssl_timeout: 5, # value for Net::HTTP#ssl_timeout=, optional
      open_timeout: 5, # value for Net::HTTP#open_timeout=, optional
      read_timeout: 5 # value for Net::HTTP#read_timeout=, optional
    )
  end
end