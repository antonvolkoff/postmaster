module HTMLCleanService
  module_function

  def clean(html)
    html_doc = Nokogiri::HTML(html)
    html_doc.css('a').each { |link| link['target'] = '_blank' }

    body = html_doc.at('body')

    script_node = Nokogiri::XML::Node.new('script', html_doc)
    script_node.content = <<-JS
      function sendHeight() {
        var height = document.body.scrollHeight;
        if (height == 0) {
          height = document.documentElement.scrollHeight;
        }

        var event = { name: 'setHeight', value: height };
        window.parent.postMessage(JSON.stringify(event), '*');
      };

      window.addEventListener('resize', sendHeight, false);
      window.addEventListener('load', sendHeight);
      document.addEventListener('DOMContentLoaded', sendHeight);
    JS
    body.add_child(script_node)

    html_doc.to_html
  end
end
