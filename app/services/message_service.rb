module MessageService
  module_function

  def list(user:, time_frame:)
    query = user.messages.within_time_range(TimeRange.new(time_frame))

    query.order(Message.arel_table[:created_at].desc)
  end

  def get(id:)
    Message.find(id)
  end
end
