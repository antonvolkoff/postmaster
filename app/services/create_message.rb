class CreateMessage
  include Wisper::Publisher

  HTML_CONTENT_TYPE = 'text/html; charset=UTF-8'.freeze

  def call(sender:, message:)
    mail = build_mail(sender, message)

    message = Message.new(
      from: sender.address, to: mail.to, data: mail.to_s, user: sender)
    message.message_tags.build(tag: outbox_tag)

    if message.save
      broadcast(:create_message_ok, message.id)
    else
      broadcast(:create_message_error, message.id)
    end
  end

  private

  def build_mail(sender, message)
    mail = Mail.new

    mail.from = sender.address
    mail.to = format_mailbox_list(message[:to])
    mail.cc = format_mailbox_list(message[:cc])
    mail.subject = message[:subject]
    mail.in_reply_to = message[:in_reply_to]

    body = render_mail(message[:body] || '')
    mail.html_part do
      content_type HTML_CONTENT_TYPE
      body body
    end

    mail
  end

  def outbox_tag
    Tag.find_by(name: 'Outbox')
  end

  def renderer
    ApplicationController.renderer
  end

  def render_mail(body)
    renderer.render(
      template: 'mails/basic',
      layout: nil,
      assigns: { body: body }
    )
  end

  # Inputs is an array of hashes which look like:
  # `{ address: 'foo@example.com', name: 'Foo Bar' }`
  def format_mailbox_list(inputs)
    inputs.map { |input| format_mailbox(input) }.join(', ')
  end

  def format_mailbox(input)
    mail_address = Mail::Address.new
    mail_address.address = input[:address]
    mail_address.display_name = input[:name]
    mail_address.to_s
  end
end