class Workspace::Create
  include Interactor

  def call
    workspace = Workspace.new(name: context.workspace_name, user: context.user)

    if save_workspace(workspace)
      context.workspace = workspace
    else
      context.fail!(message: 'Failed to create workspace')
    end
  end

  private

  def create_workspace(workspace, suffix_num = 0)
    return false if suffix_num > 9

    workspace.slug = slug(workspace.name, suffix_num)
    if !workspace.save && workspace.errors.details.keys == [:slug]
      create_workspace(workspace, suffix_num + 1)
    end

    true
  end

  def slug(name, suffix_num)
    return name.parameterize if suffix_num == 0

    "#{name.parameterize}-#{suffix_num}"
  end
end
