#
# This class goes through all messages and takes contacts out of them populates
# contacts table. This was written the first time contacts were added.
#
class Contact::Populate
  include Interactor

  def call
    Message.order(created_at: :asc).find_each do |message|
      next if message.user.nil?

      user = message.user

      mail_addresses = message.parsed[:from]&.addrs
      next if mail_addresses.nil?

      mail_addresses.each { |mail_address| handle_address(user, mail_address) }
    end
  end

  private

  def handle_address(user, mail_address)
    email = mail_address.address
    name  = mail_address.display_name

    contact = Contact.find_by(email: email, user: user)
    if contact
      contact.update(name: name)
    else
      Contact.create(user: user, email: email, name: name)
    end
  end
end
