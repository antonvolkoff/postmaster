class User::Register
  include Interactor::Organizer

  organize User::Create, Message::SendWelcome
end