class User::Create
  include Interactor

  INVITE_TOKEN_LENGTH = 10
  EMAIL_SUFFIX = '@outpostr.com'.freeze

  def call
    user = User.new(params)
    context.user = user

    ok = user.save
    context.fail! unless ok
  end

  private

  def params
    {
      address: context.username.downcase + EMAIL_SUFFIX,
      password: context.password,
      invite_token: SecureRandom.hex(INVITE_TOKEN_LENGTH),
      state: 'active'
    }
  end
end
