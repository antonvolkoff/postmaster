class User::SignIn
  include Interactor

  ADDRESS_SUFFIX = '@outpostr.com'.freeze

  def call
    address = convert_address(context.address)

    if user = User.find_by(address: address)&.authenticate(context.password)
      context.user = user
      context.user_id = user.id
    else
      context.fail!(message: 'Authentication failed')
    end
  end

  private

  def convert_address(address)
    return if address.nil?

    if address.end_with?(ADDRESS_SUFFIX)
      address.downcase
    else
      address.downcase + ADDRESS_SUFFIX
    end
  end
end
