class User::SignOut
  include Interactor

  def call
    context.user.update(auth_token: nil)
  end
end
