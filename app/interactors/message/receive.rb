class Message::Receive
  include Interactor

  after :send_notification

  def call
    context.recipient = User.find_by(address: context.to.first)
    if context.recipient.nil?
      context.fail!(error: 'Recipient does not exist')
      return
    end

    message = Message.new(
      from: context.from,
      to: context.to,
      data: context.mail.to_s,
      user: context.recipient
    )
    message.message_tags.build(tag: inbox_tag)

    if message.save
      context.message = message
    else
      context.fail!(error: 'Falled to create a message')
    end
  end

  private

  def send_notification
    WebNotificationService.send_one(context.recipient.id, 'You got a new mail')
  end

  def inbox_tag
    Tag.find_by(name: 'Inbox')
  end
end
