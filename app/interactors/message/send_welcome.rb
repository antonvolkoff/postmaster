class Message::SendWelcome
  include Interactor

  HTML_CONTENT_TYPE = 'text/html; charset=UTF-8'.freeze

  def call
    recipient = context.user

    welcome_message = {
      to: context.user.address,
      subject: 'Welcome to Outpostr!',
      body: <<-HTML
        <p>Hi and welcome to Outpostr</p>
        <p>
          Our goal is to make your email work smooth and reliable. It might be
          a challange to transition from one email service to another, so we are
          always ready to help you with it.
        </p>
        <p>
          If you need any help just reply back to this email with your question or
          contact us at hello@outpostr.com.
        </p>
        <p>Best, Outpostr Team</p>
      HTML
    }

    mail = build_mail('<hello@outpostr.com>', welcome_message)

    message = Message.new(
      from: '<hello@outpostr.com>',
      to: [recipient.address],
      data: mail.to_s,
      user: recipient
    )
    message.message_tags.build(tag: inbox_tag)

    message.save
  end

  private

  def build_mail(sender, message)
    mail = Mail.new

    mail.from = sender
    mail.to = message[:to]
    mail.subject = message[:subject]
    mail.in_reply_to = message[:in_reply_to]

    body = render_mail(message[:body])
    mail.html_part do
      content_type HTML_CONTENT_TYPE
      body body
    end

    mail
  end

  def renderer
    ApplicationController.renderer
  end

  def render_mail(body)
    renderer.render(
      template: 'mails/basic',
      layout: nil,
      assigns: { body: body }
    )
  end

  def inbox_tag
    Tag.find_by(name: 'Inbox')
  end
end
