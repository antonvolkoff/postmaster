import axios from 'axios'

// Set CSRF tokens
const csrfToken = document.querySelector("meta[name=csrf-token]").content
axios.defaults.headers.common['X-CSRF-Token'] = csrfToken

const axiosConfig = {
  responseType: 'json',
  headers: {
    'Content-Type': 'application/json'
  },
  auth: { username: window.authToken, password: '' },
}

const defaultRequestOptions = {
  method: 'post',
  responseType: 'json',
  headers: {
    'Content-Type': 'application/json'
  },
  auth: { username: window.authToken, password: '' },
}

const request = (url, data) => {
  return axios.request({
    ...defaultRequestOptions,
    url,
    data: JSON.stringify(data),
  }).then(({ data }) => {
    return data
  })
}

export default {
  message: {
    list: (query) => {
      let params = {}
      if (query.when) params.when = query.when.format('YYYY-MM-DD')

      return axios.get('/api/messages', { ...axiosConfig, params })
    },

    get: (id) => {
      return request('/api/message-get', { id })
    },

    create: (params) => {
      return request('/api/message-create', params)
    },
  },

  webNotification: {
    disable: () => {
      return request('/api/web-notification-disable', {})
    },

    enable: (subscription) => {
      return request('/api/web-notification-enable', subscription)
    },

    status: () => {
      return request('/api/web-notification-status', {})
    }
  },

  signOut() {
    return axios.post('/signout')
  },

  contacts: {
    list: (query) => {
      const params = { query: query, limit: 10 }
      return axios.get('/api/contacts', { ...axiosConfig, params })
    },
  },
}