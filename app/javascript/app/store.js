import { writable } from 'svelte/store'
import moment from 'moment'

import rpc from './rpc'

function createDaysStore() {
  const { subscribe, update } = writable([
    { num: 0, messages: [] }
  ])

  return {
    subscribe,
    load: (dayIndex) => {
      const when = moment().subtract(dayIndex, 'days')

      return rpc.message.list({ when }).then(response => {
        update(data => {
          data[dayIndex].messages = response.data.messages
          return [ ...data, { num: dayIndex + 1, messages: null, loaded: false} ]
        })
      })
    },
    markRead: (dayIndex, messageID) => {
      update(data => {
        const messages = data[dayIndex].messages
        let message = messages.find((message) => message.id == messageID)
        message.read = true

        return data
      })
    },
  }
}

export const days = createDaysStore()

export const breadcrumbs = writable([{ name: 'Today', url: '/' }])

export const menuItems = writable([
  { id: 'all', name: 'All Messages' },
])

// This variable is used to preserve scroll position on list of messages
// It's save on opening and restored when clicked back
export const scrollPosition = writable(0)

// Router
export const history = writable([])
export const url = writable('/')
export const context = writable({})
