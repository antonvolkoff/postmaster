import App from '../app/app.svelte'
import "../css/application.css"

document.addEventListener('DOMContentLoaded', () => {
  const app = new App({
    target: document.body,
    props: {}
  })

  window.app = app
})
