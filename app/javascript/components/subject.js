import { Node } from 'tiptap'

export default class extends Node {
  get name() {
    return 'subject'
  }

  get schema() {
    return {
      content: 'inline*',
      parseDOM: [{
        tag: 'h1',
      }],
      toDOM: () => ['h1', 0],
    }
  }
}
