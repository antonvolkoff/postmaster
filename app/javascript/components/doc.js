import { Doc } from 'tiptap'

export default class extends Doc {
  get schema() {
    return {
      content: 'block+',
    }
  }
}
