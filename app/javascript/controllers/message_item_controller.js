import { Controller } from 'stimulus'

export default class extends Controller {
  static targets = ['dot']

  click() {
    const readStyle = 'background-color: rgba(0, 0, 0, 0.10)'
    this.dotTarget.setAttribute('style', readStyle)
  }
}
