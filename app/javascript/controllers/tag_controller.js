import { Controller } from 'stimulus'
import Turbolinks from "turbolinks"

export default class extends Controller {
  select() {
    const tagName = this.data.get("name")
    let url = new URL(document.location)

    let tags = this.unserializeTags(url.searchParams)
    tags = this.toggleTag(tags, tagName)
    this.serializeTags(url.searchParams, tags)

    Turbolinks.visit(url.href)
  }

  toggleTag(tags, tagName) {
    if (tags.includes(tagName)) {
      tags = tags.filter(item => item !== tagName)
    } else {
      tags.push(tagName)
    }

    return tags
  }

  unserializeTags(searchParams) {
    let tags = []

    const tagsParam = searchParams.get('tags')
    if (tagsParam != null && tagsParam != '') {
      tags = tagsParam.split(',')
    }

    return tags
  }

  serializeTags(searchParams, tags) {
    if (tags.length == 0) {
      searchParams.delete('tags')
    } else {
      searchParams.set('tags', tags.join(','))
    }
  }
}