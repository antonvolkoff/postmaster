import { Controller } from 'stimulus'

export default class extends Controller {
  static targets = [ "mobileMenu", "desktopMenu" ]

  connect() {
    this.desktopMenuOpened = false
    this.bodyElement = document.body
  }

  openMobileMenu(event) {
    event.preventDefault()

    this.mobileMenuTarget.classList.remove('hidden')
    this.mobileMenuTarget.classList.add('fixed')
  }

  closeMobileMenu(event) {
    event.preventDefault()

    this.mobileMenuTarget.classList.remove('fixed')
    this.mobileMenuTarget.classList.add('hidden')
  }

  openDesktopMenu(event) {
    event.preventDefault()

    if (this.desktopMenuOpened) return

    this.desktopMenuTarget.classList.remove('hidden')
    this.desktopMenuTarget.classList.add('block')

    this.desktopMenuOpened = true

    const outsideClickListener = (event) => {
      if (!event.target.closest('.drop-down')) {
        this.closeDesktopMenu(event)
        removeClickListener()
      }
    }

    const removeClickListener = () => {
      this.bodyElement.removeEventListener('click', outsideClickListener)
    }

    this.bodyElement.addEventListener('click', outsideClickListener)
  }

  closeDesktopMenu(event) {
    event.preventDefault()

    this.desktopMenuTarget.classList.remove('block')
    this.desktopMenuTarget.classList.add('hidden')

    this.desktopMenuOpened = false
  }
}
