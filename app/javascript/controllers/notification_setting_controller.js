import { Controller } from 'stimulus'
import client from './client'
import push from './push'

export default class extends Controller {
  static targets = [ 'enabled', 'button' ]

  toggle(event) {
    event.preventDefault()

    this.isEnabled() ? this.disable() : this.enable()
  }

  enable() {
    push.requestWebPush().then(
      (pushSubscription) => { this.successHandler(pushSubscription) },
      (_error) => { /* Do nothing */ }
    )
  }

  disable() {
    client.disableWebNotification().then(() => { this.makeEnableButton() })
  }

  isEnabled() {
    return this.enabledTarget.value == 'true'
  }

  successHandler(pushSubscription) {
    client.enableWebNotification(pushSubscription).then(() => {
      this.makeDisableButton()
    })
  }

  makeDisableButton() {
    this.buttonTarget.innerText = 'Disable'
    this.buttonTarget.classList.remove('bg-green-600')
    this.buttonTarget.classList.add('bg-red-600')
    this.enabledTarget.value = 'true'
  }

  makeEnableButton() {
    this.buttonTarget.innerText = 'Enable'
    this.buttonTarget.classList.remove('bg-red-600')
    this.buttonTarget.classList.add('bg-green-600')
    this.enabledTarget.value = 'false'
  }
}
