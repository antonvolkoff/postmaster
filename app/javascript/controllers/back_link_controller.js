import { Controller } from 'stimulus'
import Turbolinks from 'turbolinks'

export default class extends Controller {
  goBack(event) {
    if (!this.isFirstVisit()) {
      event.preventDefault()
      window.history.back()
    }
  }

  isFirstVisit() {
    return (Object.keys(Turbolinks.controller.cache.snapshots).length == 0)
  }
}
