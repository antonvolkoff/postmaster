export default {
  requestWebPush() {
    const options = {
      userVisibleOnly: true,
      applicationServerKey: window.vapidPublicKey
    }

    return navigator.serviceWorker.ready.then((serviceWorkerRegistration) => {
      return serviceWorkerRegistration.pushManager.subscribe(options)
    })
  },
}
