import { Controller } from "stimulus"
import Vue from 'vue/dist/vue.esm'
import MailEditor from '../components/mail_editor.vue'
import VueJamIcons from 'vue-jam-icons'

export default class extends Controller {
  static targets = [ 'subject', 'recipients', 'content' ]

  connect() {
    Vue.use(VueJamIcons)

    this.app = new Vue({
      el: '#editor-container',
      data: () => {
        return {}
      },
      components: { MailEditor }
    })
    window.app = this.app
  }

  disconnect() {
    this.app.$destroy()
  }

  send(event) {
    event.preventDefault()

    console.log(this.subjectTarget.value)
    console.log(this.contentTarget.value)
    console.log(this.recipientsTarget.value)
    console.log('send')
  }
}
