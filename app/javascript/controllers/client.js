import axios from 'axios'

export default {
  enableWebNotification(pushSubscription) {
    return axios.post('/app/web_notification', pushSubscription)
  },

  disableWebNotification() {
    return axios.delete('/app/web_notification')
  }
}
