import { Controller } from 'stimulus'
import client from './client';
import push from './push'

export default class extends Controller {
  turnOn(event) {
    event.preventDefault()

    push.requestWebPush().then(
      (pushSubscription) => { this.successHandler(pushSubscription) },
      (error) => { this.errorHandler(error) }
    )
  }

  successHandler(pushSubscription) {
    client.enableWebNotification(pushSubscription).then(() => {
      this.hideCurrentElement()
    })
  }

  errorHandler(error) {
    this.hideCurrentElement()
  }

  hideCurrentElement() {
    this.scope.element.classList.add('hidden')
  }
}
