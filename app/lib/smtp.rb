# frozen_string_literals: true

require_relative 'smtp/domain'
require_relative 'smtp/sender'
require_relative 'smtp/server'

module SMTP
end
