module SMTP
  class Server < MidiSmtpServer::Smtpd
    ME_DOMAIN = 'outpostr.com'.freeze

    def on_message_data_event(context)
      return if context[:message][:data].nil?

      mail = Mail.read_from_string(context[:message][:data])

      from_address = Mail::Address.new(context[:envelope][:from])
      to_addresses = context[:envelope][:to].map { |to| Mail::Address.new(to) }

      if from_address.domain == ME_DOMAIN
        handle_outgoing_mail(from: from_address, to: to_addresses, mail: mail)
      else
        handle_incoming_mail(from: from_address, to: to_addresses, mail: mail)
      end

    rescue StandardError => exception
      Raven.capture_exception(exception)
      raise exception
    end

    private

    def handle_incoming_mail(from:, to:, mail:)
      Message::Receive.call(
        from: from.address,
        to: to.map(&:address),
        mail: mail
      )
    end

    def handle_outgoing_mail(from:, to:, mail:)
      return if User.find_by(address: from.address).nil?

      to.each do |address|
        exchanges = SMTP::Domain.new(email: address.address).exchanges
        next if exchanges.empty?

        SMTP::Sender
          .new(exchanges.first)
          .deliver(mail: mail, from: from, to: address)
      end
    end
  end
end
