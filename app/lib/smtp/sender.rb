module SMTP
  class Sender
    MY_NAME = 'mailbox.outpostr.com'.freeze
    PORT = 25

    def initialize
      @hostname = Rails.application.credentials.smtp_relay[:hostname]
      @username = Rails.application.credentials.smtp_relay[:username]
      @password = Rails.application.credentials.smtp_relay[:password]
    end

    def deliver(mail:, to:, from:)
      smtp = Net::SMTP.start(@hostname, PORT, MY_NAME)
      smtp.enable_starttls
      smtp.authenticate(@username, @password)
      smtp.send_message(mail.to_s, from.address, to.address)
      smtp.finish
    end
  end
end
