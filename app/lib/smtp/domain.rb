require 'resolv'

module SMTP
  class Domain
    def initialize(email:)
      @address = Mail::Address.new(email)
    end

    def exchanges
      mx_records.map { |mx| mx.exchange.to_s }
    end

    private

    def mx_records
      Resolv::DNS.open do |dns|
        dns.getresources(@address.domain, Resolv::DNS::Resource::IN::MX)
      end
    end
  end
end
