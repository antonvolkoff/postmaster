class WebNotification < ApplicationRecord
  belongs_to :user

  validates :endpoint,    presence: true
  validates :p256dh_key,  presence: true
  validates :auth_key,    presence: true
end
