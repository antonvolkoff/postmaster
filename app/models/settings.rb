class Settings
  include ActiveModel::Model

  attr_accessor :web_notification

  def self.load(user)
    Settings.new(web_notification: user.web_notification_enabled?)
  end

  def update(user)
    update_web_notification(user)
  end

  private

  def update_web_notification(user)
    return if user.web_notification_enabled? == web_notification

    if web_notification

    else

    end
  end
end