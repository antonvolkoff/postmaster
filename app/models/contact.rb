class Contact < ApplicationRecord
  belongs_to :user

  validates :email, presence: true

  scope :search, -> (input) {
    query = "%#{sanitize_sql_like(input)}%"
    where('email like :query or name like :query', query: query)
  }

  def as_json(options = {})
    { email: email, name: name }
  end
end
