class Email < SimpleDelegator
  def html_part?
    html_part.present?
  end

  def text_part?
    text_part.present?
  end

  def html_body
    html_part&.decoded || text_part&.decoded || body.to_s
  end

  def senders
    self[:from]&.addrs || []
  end

  def cc_addresses
    self[:cc]&.addrs || []
  end
end
