class User < ApplicationRecord
  has_secure_password

  STATE = [
    'waiting-list',
    'active'
  ].freeze

  has_many  :messages,          dependent: :destroy
  has_many  :user_tags,         dependent: :destroy
  has_one   :web_notification,  dependent: :destroy
  has_one   :workspace,         dependent: :destroy
  has_many  :contacts,          dependent: :destroy

  has_many  :tags,              through: :user_tags

  validates :address, presence: true
  validates :state, inclusion: { in: STATE }, allow_nil: true

  def to
    "<#{address}>"
  end

  def web_notification_enabled?
    web_notification.present?
  end
end
