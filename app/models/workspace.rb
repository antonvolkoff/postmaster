class Workspace < ApplicationRecord
  belongs_to :user

  validates :name, presence: true
  validates :slug, presence: true, uniqueness: true
end
