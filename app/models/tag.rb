class Tag < ApplicationRecord
  has_many :message_tags
  has_many :messages, through: :messaga_tags
  has_many :user_tags
  has_many :users, through: :user_tags
end
