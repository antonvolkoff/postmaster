class Message < ApplicationRecord
  belongs_to :user
  has_many :message_tags
  has_many :tags, through: :message_tags
  has_one :open_token

  validates :from, presence: true
  validates :to, presence: true
  validates :data, presence: true

  scope :within_time_range, ->(time_range) {
    where(Message.arel_table[:created_at].between(time_range.to_range))
  }

  scope :on_date, ->(date) {
    where("date(created_at) = date(?)", date)
  }

  def parsed
    @parsed ||= Email.new(Mail.read_from_string(data))
  end

  def email
    parsed
  end

  def assign!(user)
    update(user: user)
  end

  def tag_with!(name:)
    tag = Tag.find_by(name: name)
    message_tags.create!(tag: tag)
  end

  def read!
    return if read?

    touch(:read_at)
  end

  def read?
    read_at.present?
  end

  def find_attachment(filename)
    parsed.attachments.find { |a| a.filename == filename }
  end

  def as_json(options = {})
    from_value = parsed.senders.map do |sender|
      { name: sender.display_name, address: sender.address }
    end
    tags_value = tags.map { |tag| { name: tag.name, color: tag.color } }

    attachments = parsed.attachments.map do |attachment|
      { filename: attachment.filename }
    end

    {
      id: id,
      subject: parsed.subject,
      read: read?,
      from: from_value,
      arrived_at: created_at,
      tags: tags_value,
      attachments: attachments
    }
  end
end
