class TimeRange
  NAMES = %w[today yesterday this_week last_week].freeze
  STARTS_AT = {
    'today' => -> { Time.now.utc.beginning_of_day },
    'yesterday' => -> { Time.now.utc.beginning_of_day - 1.day },
    'this_week' => -> { Time.now.utc.at_beginning_of_week },
    'last_week' => -> { Time.now.utc.at_beginning_of_week - 7.days }
  }.freeze
  ENDS_AT = {
    'today' => -> { Time.now.utc },
    'yesterday' => -> { Time.now.utc.beginning_of_day },
    'this_week' => -> { Time.now.utc.at_end_of_week },
    'last_week' => -> { Time.now.utc.at_end_of_week - 7.days }
  }.freeze

  def initialize(name)
    raise 'Unsupported time frame' unless NAMES.include?(name)

    @name = name
  end

  def starts_at
    STARTS_AT[@name].call
  end

  def ends_at
    ENDS_AT[@name].call
  end

  def to_range
    starts_at..ends_at
  end
end
