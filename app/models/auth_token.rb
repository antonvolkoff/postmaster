class AuthToken
  def initialize(user)
    @user = user
  end

  def value
    return @user.auth_token if @user.auth_token

    @user.auth_token = SecureRandom.hex(32)
    @user.save

    @user.auth_token
  end
end
