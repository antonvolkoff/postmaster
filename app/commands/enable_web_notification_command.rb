class EnableWebNotificationCommand
  def initialize(user)
    @user = user
  end

  def call(args)
    params = {
      endpoint:   args[:endpoint],
      p256dh_key: args[:keys][:p256dh],
      auth_key:   args[:keys][:auth]
    }

    WebNotificationService.enable(@user.id, params)

    { ok: true, result: nil }
  end
end
