class PingCommand
  def initialize(_user); end

  def call(args)
    { ok: true, result: args }
  end
end