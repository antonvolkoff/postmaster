class WebNotificationStatusCommand
  def initialize(user)
    @user = user
  end

  def call(args)
    web_notification = WebNotification.find_by(user_id: @user.id)

    { ok: true, result: web_notification.present? }
  end
end
