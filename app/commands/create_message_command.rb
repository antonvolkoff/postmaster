class CreateMessageCommand
  def initialize(user)
    @user = user
  end

  def call(args)
    @ok = false

    create_message = CreateMessage.new
    create_message.subscribe(DeliverMessage.new, prefix: :on)

    create_message.on(:create_message_ok) { @ok = true }
    create_message.on(:create_message_error) { @ok = false }

    create_message.call(sender: @user, message: args)

    { ok: @ok }
  end
end
