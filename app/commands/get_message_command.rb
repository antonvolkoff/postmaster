class GetMessageCommand
  def initialize(user, ahoy)
    @user = user
    @ahoy = ahoy
  end

  def call(args)
    message = @user.messages.includes(:tags).find(args[:id])

    open_token = OneTimeOpenToken.generate(message.id)
    message.read!
    @ahoy.track('message_open', id: message.id)

    { ok: true, result: decorate(message, open_token) }
  end

  private

  def decorate(message, open_token)
    from = message.parsed.senders.map do |sender|
      { name: sender.display_name, address: sender.address }
    end
    cc = message.parsed.cc_addresses.map do |cc_address|
      { name: cc_address.display_name, address: cc_address.address }
    end
    tags = message.tags.map { |tag| { name: tag.name, color: tag.color } }

    attachments = message.parsed.attachments.map do |attachment|
      {
        filename: attachment.filename,
        url: attachment_url(message, attachment.filename)
      }
    end

    {
      id: message.id,
      subject: message.parsed.subject,
      read: message.read?,
      from: from,
      cc: cc,
      arrived_at: message.created_at,
      tags: tags,
      content_url: content_url(message, open_token),
      attachments: attachments
    }
  end

  def content_url(message, open_token)
    Rails.application.routes.url_helpers.app_message_content_url(
      message, open_token: open_token
    )
  end

  def attachment_url(message, filename)
    Rails.application.routes.url_helpers.api_message_attachment_url(
      message_id: message.id, id: Base64.urlsafe_encode64(filename)
    )
  end
end