class DisableWebNotificationCommand
  def initialize(user)
    @user = user
  end

  def call(args)
    WebNotificationService.disable(@user.id)

    { ok: true, result: nil }
  end
end
