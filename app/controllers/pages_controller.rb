class PagesController < ApplicationController
  def show
    if valid_page_name?
      render page_name, layout: 'web'
    else
      not_found!
    end
  end

  private

  def page_name
    params[:id]
  end

  def valid_page_name?
    %w[home thankyou pricing support sus].include? params[:id]
  end
end
