module Api
  class AttachmentsController < ApplicationController
    protect_from_forgery with: :null_session
    before_action :authenticate_with_token

    def show
      message = Message.find(params[:message_id])
      attachment = message.find_attachment(filename)

      disposition = attachment.inline? ? 'inline' : 'attachment'

      send_data attachment.decoded,
        filename: attachment.filename,
        type: attachment.content_type,
        disposition: disposition
    end

    private

    def authenticate_with_token
      authenticate_with_http_basic do |auth_token|
        @current_user = User.find_by(auth_token: auth_token)
      end
    end

    def filename
      Base64.urlsafe_decode64(params[:id])
    end
  end
end
