module Api
  class CommandsController < ApplicationController
    protect_from_forgery with: :null_session
    before_action :authenticate_with_token

    def ping
      render json: PingCommand.new(@current_user).(command_params)
    end

    def message_get
      render json: GetMessageCommand.new(@current_user, ahoy).(command_params)
    end

    def message_create
      render json: CreateMessageCommand.new(@current_user).(command_params)
    end

    def web_notification_enable
      json = EnableWebNotificationCommand.new(@current_user).(command_params)
      render json: json
    end

    def web_notification_disable
      json = DisableWebNotificationCommand.new(@current_user).(command_params)
      render json: json
    end

    def web_notification_status
      json = WebNotificationStatusCommand.new(@current_user).(command_params)
      render json: json
    end

    private

    def command_params
      params[:command]
    end

    def authenticate_with_token
      authenticate_with_http_basic do |auth_token|
        @current_user = User.find_by(auth_token: auth_token)
      end
    end
  end
end