module Api
  class MessagesController < ApplicationController
    protect_from_forgery with: :null_session
    before_action :authenticate_with_token

    def index
      @messages =
        if params[:when]
          @current_user
            .messages
            .includes(:tags)
            .on_date(Date.parse(params[:when]))
            .order(created_at: :desc)
        else
          @current_user
            .messages
            .includes(:tags)
            .order(created_at: :desc)
        end

      render json: { messages: @messages.as_json }
    end

    private

    def authenticate_with_token
      authenticate_with_http_basic do |auth_token|
        @current_user = User.find_by(auth_token: auth_token)
      end
    end
  end
end
