module Api
  class MailsController < ApiController
    ORIGIN = 'outpostr.com'.freeze

    def create
      handler =
        if mail_from_address.domain == ORIGIN
          method(:handle_outgoing_mail)
        else
          method(:handle_incoming_mail)
        end

      handler.call(
        from: mail_from_address,
        to: rcpt_to_addresses,
        mail: parsed_mail
      )

      render json: { ok: true }
    end

    private

    def handle_incoming_mail(from:, to:, mail:)
      Message::Receive.call(
        from: from.address,
        to: to.map(&:address),
        mail: mail
      )
    end

    def handle_outgoing_mail(from:, to:, mail:)
      return if User.find_by(address: from.address).nil?

      to.each do |address|
        SMTP::Sender.new.deliver(mail: mail, from: from, to: address)
      end
    end

    def mail_from_address
      @main_from_address ||= Mail::Address.new(params[:envelope][:from])
    end

    def parsed_mail
      Mail.read_from_string(params[:message][:data])
    end

    def rcpt_to_addresses
      params[:envelope][:to].map { |to| Mail::Address.new(to) }
    end
  end
end
