module Api
  class ContactsController < ApplicationController
    protect_from_forgery with: :null_session
    before_action :authenticate_with_token

    def index
      @contacts = @current_user.contacts.search(query).limit(limit)
      render json: { contacts: @contacts.as_json }
    end

    private

    def authenticate_with_token
      authenticate_with_http_basic do |auth_token|
        @current_user = User.find_by(auth_token: auth_token)
      end
    end

    def query
      params[:query] || ''
    end

    def limit
      params[:limit] || 5
    end
  end
end
