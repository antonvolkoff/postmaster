class UsersController < ApplicationController
  before_action :check_invite_token

  def new
    @user = User.new
  end

  def create
    result = User::Register.call(user_params)

    if result.success?
      track_signup_event
      redirect_to root_url
    else
      @user = result.user
      render :new
    end
  end

  private

  def user_params
    params.require(:user).permit(:username, :password)
  end

  def check_invite_token
    not_found! if User.find_by(invite_token: params[:invite_token]).nil?
  end

  def track_signup_event
    ahoy.track('sign_up')
  end
end
