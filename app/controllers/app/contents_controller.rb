module App
  class ContentsController < ApplicationController
    before_action :check_open_token

    def show
      @message = MessageService.get(id: params[:message_id])
      html = HTMLCleanService.clean(@message.parsed.html_body)

      render html: html.html_safe
    end

    private

    def check_open_token
      not_found! if params[:open_token].blank?

      check = OneTimeOpenToken.valid?(params[:message_id], params[:open_token])
      not_found! unless check
    end
  end
end
