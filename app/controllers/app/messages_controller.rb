module App
  class MessagesController < ApplicationController
    before_action :require_authentication

    def index
      @messages =
        MessageService.list(user: Current.user, time_frame: time_frame)

      @unread_count = {
        today: message_count('today'),
        yesterday: message_count('yesterday')
      }
    end

    def show
      @message = MessageService.get(id: params[:id])
      @open_token = OneTimeOpenToken.generate(@message.id)
      @message.read!
      ahoy.track('message_open', id: @message.id)
    end

    def new
      @message = Message.new
      if params[:reply_to].present?
        @reply_to_message = MessageService.get(id: params[:reply_to])
        @open_token = OneTimeOpenToken.generate(@reply_to_message.id)
      end
    end

    def create
      create_message = CreateMessage.new

      create_message.subscribe(DeliverMessage.new, prefix: :on)

      create_message.on(:create_message_ok) { redirect_to app_messages_url }
      create_message.on(:create_message_error) { render :new }

      create_message.call(sender: Current.user, message: params[:message])
    end

    private

    def require_authentication
      redirect_to signin_url if Current.user.nil?
    end

    def time_frame
      params[:when] || 'today'
    end

    def message_count(time_frame)
      Message
        .within_time_range(TimeRange.new(time_frame))
        .where(user: Current.user, read_at: nil)
        .count
    end
  end
end
