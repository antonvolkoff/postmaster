module App
  class SharedMessagesController < ApplicationController
    before_action :validate_share_code

    def show
      @message = MessageService.get(id: params[:id])
      render html: @message.parsed.html_body.html_safe
    end

    private

    def validate_share_code
      not_found! if params[:code] != 'secret'
    end
  end
end
