module App
  class SettingsController < ApplicationController
    before_action :require_authentication

    def show
      @settings = Settings.load(Current.user)
    end

    private

    def require_authentication
      redirect_to signin_url if Current.user.nil?
    end
  end
end
