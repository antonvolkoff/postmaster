module App
  class WebNotificationsController < ApplicationController
    before_action :require_authentication

    def create
      web_notification =
        WebNotificationService.enable(Current.user.id, web_notfication_params)

      json_response =
        if web_notification.valid?
          { ok: true }
        else
          { ok: false, errors: web_notification.errors }
        end

      render json: json_response
    end

    def destroy
      WebNotificationService.disable(Current.user.id)

      render json: { ok: true }
    end

    private

    def require_authentication
      redirect_to signin_url if Current.user.nil?
    end

    def web_notfication_params
      {
        endpoint:   params[:endpoint],
        p256dh_key: params[:keys][:p256dh],
        auth_key:   params[:keys][:auth]
      }
    end
  end
end
