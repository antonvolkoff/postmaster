class WannabeUsersController < ApplicationController
  def create
    wannabe_user = WannabeUser.create(wannabe_user_params)
    wannabe_user.save
    redirect_to page_url('thankyou')
  end

  private

  def wannabe_user_params
    output = { email: params[:email], meta: {} }
    output[:meta][:company_name] = params[:company_name]
    output[:meta][:name] = params[:name]
    output[:meta][:domain_name] = params[:domain_name]
    output[:meta][:domain_hosting] = params[:domain_hosting]
    output
  end
end