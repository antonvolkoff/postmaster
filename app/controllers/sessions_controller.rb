class SessionsController < ApplicationController
  def new
    redirect_to app_root_url if Current.user
  end

  def create
    result = User::SignIn.call(address: params[:address],
                               password: params[:password])

    if result.success?
      ahoy.authenticate(result.user)
      cookies.encrypted[:user_id] = { value: result.user_id, expires: 1.month }
      redirect_to app_root_url
    else
      render :new, flash: 'Wrong credentials'
    end
  end

  def destroy
    User::SignOut.call(user: Current.user)

    cookies.encrypted[:user_id] = nil
    redirect_to root_url
  end
end
