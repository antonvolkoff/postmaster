module Authentication
  extend ActiveSupport::Concern

  included do
    before_action :authenticate

    def current_user
      @current_user
    end
  end

  private

  def authenticate
    return if cookies.encrypted[:user_id].nil?
    return unless (user = User.find_by(id: cookies.encrypted[:user_id]))

    Current.user = user
    @current_user = user
  end
end