module AppV2
  class PagesController < ApplicationController
    def index
      @current_user = Current.user
      render html: nil, layout: 'app_v2'
    end
  end
end
