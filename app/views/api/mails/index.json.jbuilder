json.array! @mails do |mail|
  json.id mail.id
  json.subject mail.parsed.subject

  json.from mail.parsed.senders do |sender|
    json.address sender.address
    json.display_name sender.display_name
  end

  json.open_url api_mail_url(mail)
end
