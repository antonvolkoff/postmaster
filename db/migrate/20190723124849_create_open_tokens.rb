class CreateOpenTokens < ActiveRecord::Migration[5.2]
  def change
    create_table :open_tokens do |t|
      t.references :message, foreign_key: true
      t.string :value

      t.timestamps
    end
  end
end
