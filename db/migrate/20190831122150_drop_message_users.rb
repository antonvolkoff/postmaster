class DropMessageUsers < ActiveRecord::Migration[5.2]
  def change
    drop_table :message_users do
      t.references :message, foreign_key: true
      t.references :user, foreign_key: true
      t.datetime :read_at

      t.timestamps
    end
  end
end
