class AddReadAtToMessageUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :message_users, :read_at, :datetime
  end
end
