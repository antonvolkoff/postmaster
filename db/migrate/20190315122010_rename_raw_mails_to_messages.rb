class RenameRawMailsToMessages < ActiveRecord::Migration[5.2]
  def change
    rename_table :raw_mails, :messages
  end
end
