class DropMessages < ActiveRecord::Migration[5.2]
  def change
    drop_table :from_fields
    drop_table :to_fields
    drop_table :user_messages
    drop_table :messages
    drop_table :addresses
  end
end
