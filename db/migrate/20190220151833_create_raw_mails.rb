class CreateRawMails < ActiveRecord::Migration[5.2]
  def change
    create_table :raw_mails do |t|
      t.string :from, null: false
      t.string :to, array: true, default: []
      t.text :data, null: false

      t.timestamps
    end
  end
end
