class CreateWorkspaces < ActiveRecord::Migration[5.2]
  def change
    create_table :workspaces do |t|
      t.string :name
      t.string :slug
      t.references :user, foreign_key: true

      t.timestamps
    end

    add_index :workspaces, :slug, unique: true
  end
end
