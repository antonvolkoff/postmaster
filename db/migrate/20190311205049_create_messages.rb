class CreateMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :messages do |t|
      t.string :from
      t.string :to, array: true, default: []
      t.string :subject
      t.string :content_type
      t.text :body

      t.timestamps
    end

    add_index :messages, :to
  end
end
