class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.string :full, null: false
      t.string :address, null: false
      t.string :display_name

      t.timestamps
    end

    add_index :addresses, :address, unique: true
  end
end
