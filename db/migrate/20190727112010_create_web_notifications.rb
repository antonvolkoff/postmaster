class CreateWebNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :web_notifications do |t|
      t.references :user, foreign_key: true, index: { unique: true }
      t.text :endpoint, null: false
      t.text :p256dh_key, null: false
      t.text :auth_key, null: false

      t.timestamps
    end
  end
end
