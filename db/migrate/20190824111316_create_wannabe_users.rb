class CreateWannabeUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :wannabe_users do |t|
      t.string :email, null: false
      t.jsonb :meta, default: {}

      t.timestamps
    end
    add_index :wannabe_users, :email, unique: true
  end
end
