-- DAU
select DATE(time), count(distinct user_id)
from ahoy_events
where name = 'message_open'
group by DATE(time)
order by DATE(time) desc

-- Visits on landing page
select DATE(started_at), count(id)
from ahoy_visits
where landing_page = 'https://www.outpostr.com/'
group by DATE(started_at)
order by DATE(started_at)
