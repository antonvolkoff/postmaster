FROM ruby:2.6.3

RUN apt-get update -qq && \
    apt-get install -y postgresql-client && \
    wget -qO- https://deb.nodesource.com/setup_9.x  | bash - && \
    apt-get install -y nodejs && \
    wget -qO- https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    apt-get update && \
    apt-get install yarn && \
    apt-get clean

COPY Gemfile* /tmp/
WORKDIR /tmp
RUN bundle

# Install yarn packages
COPY package.json yarn.lock /app/
RUN yarn install

ENV app /app
WORKDIR $app
ADD . /app

# Precompile assets
RUN RAILS_ENV=production bundle exec rake assets:precompile

EXPOSE 3000

COPY ./docker-entrypoint.sh /
ENTRYPOINT [ "/docker-entrypoint.sh" ]
CMD ["rails", "server", "-b", "0.0.0.0"]